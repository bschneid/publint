'''
publint: Test module.

Meant for use with py.test.
Write each test as a function named test_<something>.
Read more here: http://pytest.org/

Copyright 2016, Basil Schneider
Licensed under GPL
'''

def test_example():
    assert True
