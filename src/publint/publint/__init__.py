'''
publint: Main module

Copyright 2016, Basil Schneider
Licensed under GPL.
'''


def main():
    '''
    Main function of the boilerplate code is the entry point of the 'publint' executable script (defined in setup.py).
    
    Use doctests, those are very helpful.
    
    >>> main()
    Hello
    >>> 2 + 2
    4
    '''
    
    print("Hello")

